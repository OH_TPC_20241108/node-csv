# text-encoding单元测试用例

该测试用例基于OpenHarmony系统下，进行单元测试

单元测试用例覆盖情况

|                                                                  接口名                                                                  |是否通过	|备注|
|:-------------------------------------------------------------------------------------------------------------------------------------:|:---:|:---:|
|                                             stringify(input: Input, callback?: Callback)                                              |    pass        |       |
|                                           generate(options?: Options, callback?: Callback)                                            |pass   |        |
|                      transform<T = any, U = any>(records: Array<T>, handler: Handler<T, U>, callback?: Callback)                      |pass   |        |
|                                                     generate(callback?: Callback)                                                     |pass   |        |




